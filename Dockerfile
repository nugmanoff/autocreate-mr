FROM alpine:3.6

RUN apk add --no-cache \
  bash \
  curl \
  grep \
  jq

COPY autocreate-mr.sh /usr/bin/

CMD ["autocreate-mr.sh"]

